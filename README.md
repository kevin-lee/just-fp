# just-fp

[![Build Status](https://github.com/Kevin-Lee/just-fp/workflows/Build-All/badge.svg)](https://github.com/Kevin-Lee/just-fp/actions?workflow=Build-All)
[![Release Status](https://github.com/Kevin-Lee/just-fp/workflows/Release/badge.svg)](https://github.com/Kevin-Lee/just-fp/actions?workflow=Release)
[![Coverage Status](https://coveralls.io/repos/github/Kevin-Lee/just-fp/badge.svg?branch=main)](https://coveralls.io/github/Kevin-Lee/just-fp?branch=main)

[![Download](https://api.bintray.com/packages/kevinlee/maven/just-fp/images/download.svg)](https://bintray.com/kevinlee/maven/just-fp/_latestVersion)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/io.kevinlee/just-fp_2.13/badge.svg)](https://search.maven.org/artifact/io.kevinlee/just-fp_2.13)
[![Latest version](https://index.scala-lang.org/kevin-lee/just-fp/just-fp/latest.svg)](https://index.scala-lang.org/kevin-lee/just-fp/just-fp)

# Please visit [https://kevin-lee.github.io/just-fp](https://kevin-lee.github.io/just-fp)
